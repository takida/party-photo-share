# 環境構築手順

## Instration

* login
```bash
$ DNS=
$ ssh -i pem/mid0111-tokyo.pem ubuntu@${DNS}
```

* Setup
```bash
$ sudo -i
# apt-get update
# apt-get install -y ca-certificates git gcc make libpcre3-dev zlib1g-dev libldap2-dev 	libssl-dev build-essential g++
```

* Install NGINX
```bash
# NGINX_VERSION=v1.7.10
# mkdir -p /var/log/nginx &&
mkdir -p /etc/nginx &&
cd ~ &&
git clone https://github.com/nginx/nginx.git &&
cd nginx &&
git checkout tags/${NGINX_VERSION}
# ./configure --with-http_ssl_module --with-debug --with-http_auth_request_module --conf-path=/etc/nginx/nginx.conf --sbin-path=/usr/sbin/nginx --pid-path=/var/log/nginx/nginx.pid --error-log-path=/var/log/nginx/error.log --http-log-path=/var/log/nginx/access.log
# make install && cd .. && rm -rf nginx
# echo "\ndaemon off;" >> /etc/nginx/nginx.conf
```

* Setup NGINX
```bash
# cat > /etc/nginx/nginx.conf
# mkdir /etc/nginx/conf.d
# cat > /etc/nginx/conf.d/default.conf
```

* MongoDB
```bash
# sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
# echo "deb http://repo.mongodb.org/apt/ubuntu "$(lsb_release -sc)"/mongodb-org/3.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.0.list
# sudo apt-get update
# sudo apt-get install -y mongodb-org
```

* Install party-photo-share(仮)
```bash
$ curl -sL https://deb.nodesource.com/setup_0.12 | sudo bash -
$ sudo apt-get install -y nodejs

$ git clone https://mid0111@bitbucket.org/mid0111/party-photo-share.git && cd party-photo-share
$ npm install && npm install bower
$ ./node_modules/bower/bin/bower install
$ ./node_modules/gulp/bin/gulp.js build
```

* Fixed node-oauth bug...
```bash
$ FILE_LIST=`find node_modules -name 'package.json' | xargs /usr/bin/grep -l '"oauth": "0.9.x"'`
$ for FILE in $FILE_LIST; do sed -i -e 's%"oauth": "0.9.x"%"oauth": "git://github.com/ciaranj/node-oauth.git#6d1b9e4b9499c73d36433be20ffd26f07eda01a0"%g' ${FILE}; done
$ for FILE in $FILE_LIST; do DIR_PATH=`echo ${FILE} | sed -e 's%\(.*\)/package.json%\1%'` && pushd ${DIR_PATH} && rm -rf ./node_modules && npm install && popd ; done
```

* Setup party-photo-share(仮)
```bash
$ sudo -i
# mv /home/ubuntu/party-photo-share/ /opt/
# chown -R root.root /opt/party-photo-share/
# mkdir -p /opt/party-photo-share/build/img/uploads/th
# mkdir -p /opt/party-photo-share/build/img/uploads/m
# echo "127.0.0.1 nodejs" >> /etc/hosts
```

* Start
```bash
# nginx
# cd /opt/party-photo-share/
# MONGO_HOST=localhost node app.js
```

## IPアドレス

* EIP 取得
* Facebook URL 更新
* Twitter URL 更新
* Github URL 更新
