var request = require('supertest');
var express = require('express');
var fs = require('fs');

var app = require('./app');

describe('GET /', function() {

  it('ホーム画面を取得できること', function(done) {
    request(app)
      .get('/')
      .expect(200)
      .expect('Content-Type', 'text/html; charset=utf-8')
      .end(function(err, res){
        if (err) throw err;
        done();
      });
  });
});

