var express = require('express');
var router = express.Router();
var passport = require('passport');

/* GET users listing. */
router.get('/facebook', passport.authenticate('facebook'));
router.get('/facebook/callback', passport.authenticate('facebook', {
  successRedirect: '/',
  failureRedirect: '/' 
}));

router.get('/twitter', passport.authenticate('twitter'));
router.get('/twitter/callback', passport.authenticate('twitter', {
  successRedirect: '/',
  failureRedirect: '/'
}));

router.get('/github', passport.authenticate('github'));
router.get( '/github/callback', passport.authenticate( 'github', { 
  successRedirect: '/',
  failureRedirect: '/'
}));

router.get('/signup', function(req, res) {
  if(req.user) {
    req.logout();
  }
  return res.render('signup');
});
router.post('/signup', function(req, res) {
  var mongoose = require('mongoose');
  var User = mongoose.model('User');

  var user = new User();
  user.originalId = req.body.email;
  user.name = req.body.nickname;
  user.password = req.body.password;
  User.findOne({originalId: user.originalId}, function(err, doc) {
    if (err) {
      console.error('Failed to search user.' + err);
    }
    if (doc) {
      console.log('User Already registered.');
      return passport.authenticate('local')(req, res, function (){
        res.redirect('/');
      });
    }
    user.save(function(err) {
      if(err) {
        console.error('Failed to regist user. ' + err);
        return res.redirect('/');
      }
      console.log('Regist completed.');
      return passport.authenticate('local')(req, res, function (){
        res.redirect('/');
      });
    });
  });
});

router.get('/login', function(req, res) {
  if(req.user) {
    req.logout();
  }
  return res.render('login');
});
router.post('/login', passport.authenticate('local', {
  successRedirect: '/',
  failureRedirect: '/auth/login'
}));

router.get('/logout', function(req, res) {
  req.logout();
  res.redirect('/');
});

module.exports = router;
