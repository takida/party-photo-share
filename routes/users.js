var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User = mongoose.model('User');
var Photo = mongoose.model('Photo');

router.get('/', function(req, res) {
  User.findById(req.user._id)
    .exec()
    .then(function(user) {
      res.send({
        user: user
      });
    }, function(err) {
      console.error('Failed to find user. ' + err);
    });
});

router.get('/star', function(req, res) {
  console.log('query: ' + JSON.stringify(req.query));
  var showIndex = req.query.show;

  if(showIndex) {
    User.findById(req.user._id).exec()
      .then(function(user) {
        return findPhotoByIds(user.star);
      })
      .then(function(photos) {
        if(showIndex >= photos.length) {
          res.send({
            photos: []
          });
        }
        res.send({
          photos: photos.slice(showIndex, showIndex + 30)
        });
      }, function(err) {
        console.error('Failed to populate photos. ' + err);
      });
    return;
  }

  res.render('user-photos');
});

function findPhotoByIds(ids) {
  return Photo.find({ _id: { $in: ids}})
    .populate('author', 'name')
    .exec();
}

module.exports = router;
