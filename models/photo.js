var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var photoSchema = new Schema({
  name: String,
  path: String,
  thumb: String,
  originalname: String,
  mimetype: String,
  width: Number,
  height: Number,
  orientation: Number,
  createdat: Date,
  starCount: Number,
  author: {type: Schema.Types.ObjectId, ref: 'User'}
});

var Photo = mongoose.model('Photo', photoSchema);
