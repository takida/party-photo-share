var React = require('react');
var NavMenu = require('./nav-menu.js');

module.exports = {
  MainLogo : React.createClass({
    render: function() {
      return (
          <div>
          <img className="logo animated bounce" src="/img/logo.png"/>
          </div>
      );
    }
  }),

  MinLogo: React.createClass({
    getInitialState: function() {
      return {
        openMenu: false
      };
    },

    toggleMenu: function() {
      this.setState({
        openMenu: !this.state.openMenu
      });
    },

    render: function() {
      var hidden = true;
      if(this.state.openMenu) {
        hidden = false;
      }
      return (
          <div>
          <i id="humberger-button" className="fa fa-bars fa-2x" onClick={this.toggleMenu} />
          <div className="logo-min-frame center">
          <a href="/">
          <img className="logo-min" src="/img/logo.png"/>
          </a>
          </div>
          <NavMenu open={this.state.openMenu} />
          <div className="slidenav-content" onClick={this.toggleMenu} hidden={hidden}></div>
          </div>
      );
    }
  })
};
