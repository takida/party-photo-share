var React = require('react');
var Photo = require('./photo-thumb');

module.exports = React.createClass({
  propTypes: {
    index: React.PropTypes.number.isRequired,
    onZoom: React.PropTypes.func.isRequired,
    photos: React.PropTypes.array.isRequired,
    stars: React.PropTypes.array.isRequired,
    handleOnStar: React.PropTypes.func.isRequired
  },

  render: function() {
    var photos = this.props.photos.map(function(photo) {
      var isStared = this.props.stars.indexOf(photo._id) >= 0;
      return (
          <Photo _id={photo._id}
        name={photo.name}
        originalname={photo.originalname}
        createdat={photo.createdat}
        width={photo.width}
        height={photo.height}
        author={photo.author}
        starCount={photo.starCount}
        isStared={isStared}
        key={photo._id}
        onZoom={this.props.onZoom}
        handleOnStar={this.props.handleOnStar}
          />
      );
    }.bind(this));
    return (
        <div>
        {photos}
      </div>
    );
  }
});

