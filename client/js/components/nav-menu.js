var React = require('react');
var SideNav = require("react-sidenav");

module.exports = React.createClass({

  propTypes: {
    open: React.PropTypes.bool.isRequired
  },

  render: function() {
    var className = 'sidenav';
    if(this.props.open) {
      className += ' open';
    }
    return(
        <SideNav.Menu itemHeight="50px" className={className}>

        <SideNav.MenuItem itemKey="/">
        <SideNav.ILeftItem className="fa fa-home">
        ホーム
      </SideNav.ILeftItem>
        </SideNav.MenuItem>
        <SideNav.MenuItem itemKey="/photos?sort=createdat">
        <SideNav.ILeftItem className="fa fa-newspaper-o">
        新着フォト
      </SideNav.ILeftItem>
        </SideNav.MenuItem>
        <SideNav.MenuItem itemKey="/photos?sort=starCount">
        <SideNav.ILeftItem className="fa fa-cubes">
        みんなのお気に入り
      </SideNav.ILeftItem>
        </SideNav.MenuItem>
        <SideNav.MenuItem itemKey="/users/star">
        <SideNav.ILeftItem className="fa fa-star">
        お気に入り
      </SideNav.ILeftItem>
        </SideNav.MenuItem>
        <SideNav.MenuItem itemKey="/auth/logout">
        <SideNav.ILeftItem className="fa fa-sign-out">
        ログアウト
      </SideNav.ILeftItem>
        </SideNav.MenuItem>
        </SideNav.Menu>
    );
  }
});
