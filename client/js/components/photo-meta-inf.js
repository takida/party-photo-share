var React = require('react');
var request = require('request');
var PhotoZoom = require('./photo-zoom');
var NavNextPhoto = require('./nav-photo');

module.exports = React.createClass({
  propTypes: {
    author: React.PropTypes.string.isRequired,
    starCount: React.PropTypes.number.isRequired,
    isStared: React.PropTypes.bool.isRequired,
    handleOnStar: React.PropTypes.func.isRequired
  },

  handleOnStar: function() {
    this.props.handleOnStar();
  },

  render: function() {
    var starImg = '/img/icon-unstar.png';
    if(this.props.isStared) {
      starImg = '/img/icon-star.png';
    }
    return (
        <div className="photo-meta">
        <div className="photo-meta-inf photo-author">by {this.props.author}</div>
        <div className="photo-meta-inf photo-comment">{this.props.starCount} stars </div>
        <img className="photo-meta-inf icon" src={starImg} onClick={this.handleOnStar} />
        </div>
    );
  }
});
