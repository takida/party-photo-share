var React = require('react');
var MinLogo = require('./components/logo.js').MinLogo;
var PhotoBox = require('./components/photo-box.js');

var sortOption = 'starCount';

React.render(<MinLogo />, document.getElementById('title-frame-min'));
React.render(<PhotoBox sortOption={sortOption} isUserMode={true} />, document.getElementById('photo-frame'));
