var React = require('react');
var MinLogo = require('./components/logo.js').MinLogo;
var PhotoBox = require('./components/photo-box.js');

var sortOption = document.getElementById('photo-sort-option').innerHTML;

React.render(<MinLogo />, document.getElementById('title-frame-min'));
React.render(<PhotoBox sortOption={sortOption} isUserMode={false} />, document.getElementById('photo-frame'));
