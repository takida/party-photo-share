var $ = require('jquery');
var React = require('react');
var MainLogo = require('./components/logo.js').MainLogo;
var MinLogo = require('./components/logo.js').MinLogo;

var home = document.querySelector('.home');
var main = document.querySelector('.main');
var filesInput = document.getElementById('input-file-upload');

// main
React.render(<MainLogo />, document.getElementById('title-frame'));

var uploadIcon = document.getElementById("icon-upload");
uploadIcon.onclick = function() {
  home.hidden = true;
  main.hidden = false;

  document.getElementById('icon-loading').hidden = false;
  document.body.onfocus = checkFilesInput;

  filesInput.click();
};

var galleryIcon = document.getElementById("icon-gallery");
galleryIcon.onclick = function() {
  window.location = "/photos?sort=createdat";
};

function checkFilesInput() {
  if(!filesInput.value.length) {
    document.getElementById('icon-loading').hidden = true;
    document.body.onfocus = null;
  }
}

// Upload Dialog
React.render(<MinLogo />, document.getElementById('title-frame-min'));
filesInput.onchange = function(event) {
  document.body.onfocus = null;

  var file = event.target.files[0];
  var block = document.createElement("div");
  block.classList.add('picture');
  block.classList.add('pure-u-1');
  block.classList.add('pure-u-sm-1-3');
  document.getElementById('show-pictures').appendChild(block);

  var options = { };
  loadImage.parseMetaData(file, function (data) {
    if (data.exif) {
      options.orientation = data.exif.get('Orientation');
    }
    loadImage(file, function (img) {
      img.classList.add('upload-picture');
      block.appendChild(img);
      document.getElementById('icon-loading').hidden = true;

      if(img.toDataURL() === 'data:,') {
        // ios 8 ? で画像が表示されない問題への対応
        var img2 = document.createElement("img");
        var reader = new window.FileReader();
        reader.onload = (function(aImg) { return function(e) { aImg.src = e.target.result; }; })(img2);
        reader.readAsDataURL(file);
        img2.classList.add('upload-picture');
        block.removeChild(block.childNodes[0]);
        block.appendChild(img2);
      }
    }, options);
  });
};

document.getElementById('button-cancel').onclick = function(){
  $('#show-pictures').empty();

  home.hidden = false;
  main.hidden = true;

  document.getElementById('icon-loading').hidden = true;
};
